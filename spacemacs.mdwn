# Spacemacs as a source of ITPs

[Spacemacs](http://spacemacs.org/) is a very popular distribution of
Emacs whose current install process---replace `~/.emacs` with the
spacemacs repo---is less than ideal.  It would be good to one day
package it for Debian, perhaps providing a `/usr/bin/spacemacs`
wrapper so that spacemacs can coexist with a user's regular Emacs
configuration.

Before we can do this we need to package all the ELPA packages that
Spacemacs "configuration layers" pull in.  As Thomas Koch has
suggested, this is independently a good idea because the packages that
a standard Spacemacs configuration pulls in tend to be the more useful
and higher quality ones.

## List of ELPA packages used by Spacemacs

[This
script](https://anonscm.debian.org/cgit/users/dogsleg/spacemacs-pkgs.git/)
reads the Spacemacs source tree and extracts a list of dependencies.

Someone should write a script to extract the list from the Spacemacs
repo.  Look in all files called `packages.el` for code like `(setq
foo-packages '(foo bar baz))`.

This should be split that into two lists: the ELPA dependencies of the
configuration layers activated by default in Spacemacs, and the ELPA
dependencies of optional configuration layers.  Although we probably
won't package Spacemacs until all the dependencies are packaged, it is
useful to prioritise by splitting the list this way.

We should also filter out things that we've already packaged.

See also #828154.

Output of running the script as of 8 January 2018:

[[!table  data="""
Package|Packaged by pkg-emacsen-addons?|Layers
2048-game|todo|+fun/games
ac-ispell|todo|+completion/auto-completion
ace-jump-helm-line|todo|+completion/helm
ace-pinyin|todo|+intl/chinese
ace-window|**DONE**|+distributions/spacemacs-base
adaptive-wrap|todo|+spacemacs/spacemacs-editing-visual
adoc-mode|todo|+lang/asciidoc
afternoon-theme|todo|+themes/themes-megapack
alchemist|todo|+lang/elixir
alect-themes|todo|+themes/themes-megapack
ample-theme|todo|+themes/themes-megapack
ample-zen-theme|todo|+themes/themes-megapack
anaconda-mode|todo|+lang/python
ansible-doc|todo|+tools/ansible
anti-zenburn-theme|todo|+themes/themes-megapack
apropospriate-theme|todo|+themes/themes-megapack
arduino-mode|todo|+lang/extra-langs
asm-mode|todo|+lang/asm
async|**DONE**|+distributions/spacemacs-bootstrap
auctex|todo|+lang/latex
auctex-latexmk|todo|+lang/latex
auto-compile|todo|+lang/emacs-lisp
auto-complete|todo|+completion/auto-completion
auto-dictionary|todo|+checkers/spell-checking
auto-highlight-symbol|todo|+completion/helm, +completion/ivy, +spacemacs/spacemacs-editing-visual
auto-yasnippet|todo|+completion/auto-completion
avy|**DONE**|+spacemacs/spacemacs-editing
badwolf-theme|todo|+themes/themes-megapack
bind-key|**DONE**|+distributions/spacemacs-bootstrap
bind-map|**DONE**|+distributions/spacemacs-bootstrap
birds-of-paradise-plus-theme|todo|+themes/themes-megapack
bracketed-paste|todo|+spacemacs/spacemacs-editing
bubbleberry-theme|todo|+themes/themes-megapack
bundler|todo|+lang/ruby
busybee-theme|todo|+themes/themes-megapack
cargo|todo|+lang/rust
centered-buffer-mode|todo|+distributions/spacemacs-base
centered-cursor|todo|+spacemacs/spacemacs-ui
cherry-blossom-theme|todo|+themes/themes-megapack
chinese-pyim|todo|+intl/chinese
chinese-wbim|todo|+intl/chinese
chruby|todo|+lang/ruby
cider|todo|+lang/clojure
cider-eval-sexp-fu|todo|+lang/clojure
clean-aindent-mode|todo|+spacemacs/spacemacs-editing
clj-refactor|todo|+lang/clojure
clojure-mode|**DONE**|+lang/clojure
clojure-snippets|todo|+lang/clojure
clues-theme|todo|+themes/themes-megapack
cmm-mode|todo|+lang/haskell
coffee-mode|todo|+lang/javascript
color-identifiers-mode|todo|+themes/colors
color-theme-sanityinc-solarized|todo|+themes/themes-megapack
color-theme-sanityinc-tomorrow|todo|+themes/themes-megapack
column-enforce-mode|todo|+spacemacs/spacemacs-editing-visual
common-lisp-snippets|todo|+lang/common-lisp
company|**DONE**|+completion/auto-completion, +emacs/org, +frameworks/react, +tools/shell, +tools/puppet, +tools/ansible, +tools/finance, +tools/restclient, +chat/erc, +chat/rcirc, +lang/python, +lang/csharp, +lang/agda, +lang/javascript, +lang/rust, +lang/clojure, +lang/latex, +lang/shell-scripts, +lang/elixir, +lang/haskell, +lang/ruby, +lang/html, +lang/ocaml, +lang/java, +lang/erlang, +lang/markdown, +lang/scheme, +lang/shaders, +lang/d, +lang/go, +lang/lua, +lang/php, +lang/typescript, +lang/racket, +lang/emacs-lisp, +lang/elm, +os/nixos
company-anaconda|todo|+lang/python
company-ansible|todo|+tools/ansible
company-auctex|todo|+lang/latex
company-cabal|todo|+lang/haskell
company-dcd|todo|+lang/d
company-emacs-eclim|todo|+lang/java
company-emoji|todo|+fun/emoji, +emacs/org, +chat/erc, +chat/rcirc, +lang/markdown
company-ghc|todo|+lang/haskell
company-ghci|todo|+lang/haskell
company-glsl|todo|+lang/shaders
company-go|todo|+lang/go
company-nixos-options|todo|+os/nixos
company-quickhelp|todo|+completion/auto-completion, +lang/racket
company-restclient|todo|+tools/restclient
company-shell|todo|+lang/shell-scripts
company-statistics|todo|+completion/auto-completion
company-tern|todo|+frameworks/react, +lang/javascript
company-web|todo|+lang/html
company-ycmd|todo|+tools/ycmd
counsel|**DONE**|+completion/ivy
counsel-dash|todo|+tools/dash
counsel-projectile|todo|+completion/ivy
csharp-mode|todo|+lang/csharp
css-mode|todo|+lang/html
csv-mode|todo|+lang/csv
cyberpunk-theme|todo|+themes/themes-megapack
cython-mode|todo|+lang/python
d-mode|todo|+lang/d
dactyl-mode|todo|+lang/vimscript
dakrone-theme|todo|+themes/themes-megapack
darkburn-theme|todo|+themes/themes-megapack
darkmine-theme|todo|+themes/themes-megapack
darkokai-theme|todo|+themes/themes-megapack
darktooth-theme|todo|+themes/themes-megapack
desktop|todo|+spacemacs/spacemacs-ui
diff-hl|**DONE**|+vim/vinegar, +source-control/version-control
diff-mode|todo|+source-control/version-control
diminish|**DONE**|+distributions/spacemacs-bootstrap
django-theme|todo|+themes/themes-megapack
dos|todo|+lang/windows-scripts
dracula-theme|todo|+themes/themes-megapack
drupal-mode|todo|+lang/php
dumb-jump|todo|+spacemacs/spacemacs-misc
eclim|todo|+lang/java
ein|todo|+lang/ipython-notebook
elfeed-goodies|todo|+web-services/elfeed
elfeed-org|todo|+web-services/elfeed
elfeed-web|**DONE**|+web-services/elfeed
elisp-slime-nav|**DONE**|+lang/emacs-lisp
elixir-mode|todo|+lang/elixir
elm-mode|todo|+lang/elm
emmet-mode|todo|+frameworks/react, +lang/html
emoji-cheat-sheet-plus|todo|+fun/emoji, +emacs/org, +chat/erc, +chat/rcirc, +lang/markdown
engine-mode|**DONE**|+web-services/search-engine
enh-ruby-mode|todo|+lang/ruby
ensime|todo|+lang/scala
erc|todo|+chat/erc
erc-gitter|todo|+chat/erc
erc-hl-nicks|todo|+chat/erc
erc-image|todo|+chat/erc
erc-sasl|todo|+chat/erc
erc-social-graph|todo|+chat/erc
erc-tex|todo|+chat/erc
erc-view-log|todo|+chat/erc
erc-yank|todo|+chat/erc
erc-yt|todo|+chat/erc
erlang|todo|+lang/erlang
esh-help|**DONE**|+tools/shell
eshell-prompt-extras|**DONE**|+tools/shell
eshell-z|todo|+tools/shell
espresso-theme|todo|+themes/themes-megapack
ess|todo|+lang/ess
ess-R-data-view|todo|+lang/ess
ess-R-object-popup|todo|+lang/ess
ess-smart-equals|todo|+lang/ess
eval-sexp-fu|todo|+spacemacs/spacemacs-editing
evil|**DONE**|+completion/ivy, +lang/emacs-lisp, +distributions/spacemacs-bootstrap
evil-args|todo|+spacemacs/spacemacs-evil
evil-ediff|todo|+spacemacs/spacemacs-evil
evil-escape|todo|+distributions/spacemacs-base
evil-evilified-state|todo|+distributions/spacemacs-base
evil-exchange|todo|+spacemacs/spacemacs-evil
evil-iedit-state|todo|+spacemacs/spacemacs-evil
evil-indent-plus|todo|+spacemacs/spacemacs-evil
evil-lisp-state|todo|+spacemacs/spacemacs-evil
evil-magit|todo|+source-control/git
evil-matchit|todo|+frameworks/react, +spacemacs/spacemacs-evil, +lang/python, +lang/csharp, +lang/javascript, +lang/latex, +lang/ruby, +lang/html
evil-mc|todo|+spacemacs/spacemacs-evil
evil-nerd-commenter|todo|+vim/evil-commentary, +spacemacs/spacemacs-evil
evil-numbers|todo|+spacemacs/spacemacs-evil
evil-org|todo|+emacs/org
evil-search-highlight-persist|todo|+spacemacs/spacemacs-evil
evil-snipe|todo|+vim/evil-snipe
evil-surround|todo|+emacs/org, +spacemacs/spacemacs-evil
evil-tutor|todo|+spacemacs/spacemacs-evil
evil-unimpaired|todo|+source-control/version-control, +spacemacs/spacemacs-evil
evil-visual-mark-mode|todo|+spacemacs/spacemacs-evil
evil-visualstar|todo|+distributions/spacemacs-base
exec-path-from-shell|todo|+os/osx, +distributions/spacemacs-base
expand-region|**DONE**|+spacemacs/spacemacs-editing
fancy-battery|todo|+spacemacs/spacemacs-ui-visual
farmhouse-theme|todo|+themes/themes-megapack
fasd|todo|+tools/fasd
fcitx|todo|+intl/chinese
feature-mode|todo|+frameworks/ruby-on-rails
fill-column-indicator|**DONE**|+source-control/git, +spacemacs/spacemacs-ui-visual
find-by-pinyin-dired|todo|+intl/chinese
firebelly-theme|todo|+themes/themes-megapack
fish-mode|todo|+lang/shell-scripts
flatland-theme|todo|+themes/themes-megapack
flatui-theme|todo|+themes/themes-megapack
floobits|todo|+pair-programming/floobits
flx|**DONE**|+completion/ivy
flx-ido|**DONE**|+spacemacs/spacemacs-ui
flycheck|**DONE**|+frameworks/react, +tools/puppet, +checkers/syntax-checking, +lang/swift, +lang/python, +lang/javascript, +lang/rust, +lang/latex, +lang/shell-scripts, +lang/elixir, +lang/haskell, +lang/ruby, +lang/nim, +lang/html, +lang/erlang, +lang/d, +lang/go, +lang/lua, +lang/php, +lang/purescript, +lang/typescript, +lang/emacs-lisp, +lang/scala, +lang/elm
flycheck-credo|todo|+lang/elixir
flycheck-dmd-dub|todo|+lang/d
flycheck-elm|todo|+lang/elm
flycheck-gometalinter|todo|+lang/go
flycheck-haskell|todo|+lang/haskell
flycheck-ledger|todo|+tools/finance
flycheck-mix|todo|+lang/elixir
flycheck-nim|todo|+lang/nim
flycheck-pos-tip|todo|+checkers/syntax-checking
flycheck-rust|todo|+lang/rust
flycheck-ycmd|todo|+tools/ycmd
flymd|todo|+tools/chrome
flyspell|todo|+chat/rcirc, +checkers/spell-checking, +lang/latex
flyspell-correct|todo|+checkers/spell-checking
flyspell-correct-helm|todo|+checkers/spell-checking
flyspell-correct-ivy|todo|+checkers/spell-checking
fsharp-mode|todo|+lang/fsharp
fuzzy|todo|+completion/auto-completion
gandalf-theme|todo|+themes/themes-megapack
geeknote|todo|+web-services/evernote
geiser|**DONE**|+lang/scheme
ggtags|todo|+lang/asm, +lang/windows-scripts, +lang/python, +lang/csharp, +lang/common-lisp, +lang/javascript, +lang/rust, +lang/clojure, +lang/latex, +lang/shell-scripts, +lang/elixir, +lang/haskell, +lang/ruby, +lang/ocaml, +lang/java, +lang/erlang, +lang/scheme, +lang/d, +lang/go, +lang/lua, +lang/php, +lang/racket, +lang/emacs-lisp, +lang/vimscript, +lang/octave, +lang/scala, +lang/fsharp
gh-md|todo|+lang/markdown
ghc|todo|+lang/haskell
gist|todo|+source-control/github
git-commit|**DONE**|+source-control/git
git-gutter|todo|+source-control/version-control
git-gutter+|todo|+source-control/version-control
git-gutter-fringe|todo|+source-control/version-control
git-gutter-fringe+|todo|+source-control/version-control
git-link|todo|+source-control/git
git-messenger|**DONE**|+source-control/git
git-timemachine|**DONE**|+source-control/git
gitattributes-mode|**DONE**|+source-control/git
gitconfig-mode|**DONE**|+source-control/git
github-browse-file|todo|+source-control/github
github-clone|todo|+source-control/github
github-search|todo|+source-control/github
gitignore-mode|**DONE**|+source-control/git
gmail-message-mode|todo|+tools/chrome
gnuplot|todo|+emacs/org
gnus|todo|+email/gnus
go-eldoc|todo|+lang/go
go-guru|todo|+lang/go
go-mode|todo|+lang/go
go-rename|todo|+lang/go
golden-ratio|**DONE**|+tools/tmux, +tools/restclient, +spacemacs/spacemacs-ui-visual, +lang/idris, +lang/ess
gotham-theme|todo|+themes/themes-megapack
grandshell-theme|todo|+themes/themes-megapack
gruber-darker-theme|todo|+themes/themes-megapack
gruvbox-theme|todo|+themes/themes-megapack
haml-mode|todo|+lang/html
haskell-mode|**DONE**|+lang/haskell
haskell-snippets|todo|+lang/haskell
hc-zenburn-theme|todo|+themes/themes-megapack
helm|**DONE**|+completion/helm, +tools/shell, +spacemacs/spacemacs-layouts, +lang/common-lisp, +os/osx
helm-ag|**DONE**|+completion/helm
helm-c-yasnippet|todo|+completion/auto-completion
helm-company|todo|+completion/auto-completion
helm-cscope|todo|+tags/cscope, +lang/python
helm-css-scss|todo|+lang/html
helm-dash|todo|+tools/dash
helm-descbinds|todo|+completion/helm
helm-flx|todo|+completion/helm
helm-games|todo|+fun/games
helm-gitignore|todo|+source-control/git
helm-gtags|todo|+lang/asm, +lang/windows-scripts, +lang/python, +lang/csharp, +lang/common-lisp, +lang/javascript, +lang/rust, +lang/clojure, +lang/latex, +lang/shell-scripts, +lang/elixir, +lang/haskell, +lang/ruby, +lang/ocaml, +lang/java, +lang/erlang, +lang/scheme, +lang/d, +lang/go, +lang/lua, +lang/php, +lang/racket, +lang/emacs-lisp, +lang/vimscript, +lang/octave, +lang/scala, +lang/fsharp
helm-hoogle|todo|+lang/haskell
helm-make|todo|+completion/helm, +completion/ivy
helm-mode-manager|todo|+completion/helm
helm-nixos-options|todo|+os/nixos
helm-projectile|**DONE**|+completion/helm
helm-pydoc|todo|+lang/python
helm-rcirc|todo|+chat/rcirc
helm-spacemacs-faq|todo|+completion/helm
helm-spacemacs-help|todo|+completion/helm
helm-spotify|todo|+web-services/spotify
helm-swoop|todo|+completion/helm
helm-themes|todo|+completion/helm
help-fns+|todo|+distributions/spacemacs-base
hemisu-theme|todo|+themes/themes-megapack
heroku-theme|todo|+themes/themes-megapack
hide-comnt|todo|+spacemacs/spacemacs-editing-visual
highlight-indentation|**DONE**|+spacemacs/spacemacs-editing-visual
highlight-numbers|**DONE**|+spacemacs/spacemacs-editing-visual
highlight-parentheses|todo|+spacemacs/spacemacs-editing-visual
hindent|todo|+lang/haskell
hippie-exp|todo|+completion/auto-completion
hl-anything|todo|+spacemacs/spacemacs-editing-visual
hl-todo|**DONE**|+spacemacs/spacemacs-ui-visual
hlint-refactor|todo|+lang/haskell
holy-mode|todo|+distributions/spacemacs-base
htmlize|**DONE**|+emacs/org
hungry-delete|**DONE**|+spacemacs/spacemacs-editing
hy-mode|todo|+lang/python
hybrid-mode|todo|+distributions/spacemacs-base
hydra|**DONE**|+distributions/spacemacs-bootstrap
ibuffer|todo|+emacs/ibuffer
ibuffer-projectile|**DONE**|+emacs/ibuffer
ido-vertical-mode|**DONE**|+spacemacs/spacemacs-completion
indent-guide|todo|+spacemacs/spacemacs-editing-visual
info+|todo|+spacemacs/spacemacs-ui
inkpot-theme|todo|+themes/themes-megapack
insert-shebang|todo|+lang/shell-scripts
intero|todo|+lang/haskell
ir-black-theme|todo|+themes/themes-megapack
ivy|**DONE**|+completion/ivy, +spacemacs/spacemacs-layouts
ivy-hydra|**DONE**|+completion/ivy
ivy-spacemacs-help|todo|+completion/ivy
jabber|**DONE**|+chat/jabber
jazz-theme|todo|+themes/themes-megapack
jbeans-theme|todo|+themes/themes-megapack
jinja2-mode|**DONE**|+tools/ansible
js-doc|todo|+frameworks/react, +lang/javascript
js2-mode|todo|+lang/javascript
js2-refactor|todo|+lang/javascript
json-mode|todo|+lang/javascript
json-snatcher|todo|+lang/javascript
julia-mode|todo|+lang/extra-langs
launchctl|todo|+os/osx
ledger-mode|todo|+tools/finance
less-css-mode|todo|+lang/html
light-soap-theme|todo|+themes/themes-megapack
link-hint|todo|+spacemacs/spacemacs-editing
linum-relative|**DONE**|+spacemacs/spacemacs-evil
live-py-mode|todo|+lang/python
livid-mode|todo|+lang/javascript
lorem-ipsum|todo|+spacemacs/spacemacs-editing
lua-mode|**DONE**|+lang/lua
lush-theme|todo|+themes/themes-megapack
macrostep|todo|+lang/emacs-lisp
madhat2r-theme|todo|+themes/themes-megapack
magit|**DONE**|+vim/evil-snipe, +tools/shell, +source-control/git
magit-gh-pulls|todo|+source-control/github
magit-gitflow|todo|+source-control/git
majapahit-theme|todo|+themes/themes-megapack
markdown-mode|**DONE**|+lang/markdown
markdown-toc|todo|+lang/markdown
material-theme|todo|+themes/themes-megapack
matlab-mode|todo|+lang/extra-langs
merlin|todo|+lang/ocaml
minimal-theme|todo|+themes/themes-megapack
minitest|todo|+lang/ruby
mmm-mode|todo|+lang/markdown
moe-theme|todo|+themes/themes-megapack
molokai-theme|todo|+themes/themes-megapack
monochrome-theme|todo|+themes/themes-megapack
monokai-theme|**DONE**|+themes/themes-megapack
move-text|**DONE**|+spacemacs/spacemacs-editing
mu4e|todo|+emacs/org
multi-term|todo|+tools/shell
mustang-theme|todo|+themes/themes-megapack
naquadah-theme|todo|+themes/themes-megapack
nasm-mode|todo|+lang/asm
neotree|todo|+spacemacs/spacemacs-ui-visual
niflheim-theme|todo|+themes/themes-megapack
nix-mode|todo|+os/nixos
nixos-options|todo|+os/nixos
noctilux-theme|todo|+themes/themes-megapack
noflet|**DONE**|+lang/scala
nose|**DONE**|+lang/python
nyan-mode|todo|+themes/colors
ob-elixir|todo|+lang/elixir
ob-http|todo|+tools/restclient
ob-restclient|todo|+tools/restclient
ob-sml|todo|+lang/sml
obsidian-theme|todo|+themes/themes-megapack
occidental-theme|todo|+themes/themes-megapack
ocp-indent|todo|+lang/ocaml
oldlace-theme|todo|+themes/themes-megapack
omnisharp|todo|+lang/csharp
omtose-phellack-theme|todo|+themes/themes-megapack
open-junk-file|todo|+spacemacs/spacemacs-ui
org|**DONE**|+emacs/org, +tools/shell, +intl/chinese, +lang/python, +lang/clojure, +lang/scala
org-download|todo|+emacs/org
org-pomodoro|todo|+emacs/org
org-present|todo|+emacs/org
org-projectile|todo|+emacs/org
organic-green-theme|todo|+themes/themes-megapack
orgit|todo|+source-control/git
origami|todo|+spacemacs/spacemacs-editing
osx-dictionary|todo|+os/osx
osx-location|todo|+tools/geolocation
osx-trash|todo|+os/osx
ox-gfm|todo|+emacs/org
ox-pandoc|todo|+tools/pandoc
ox-reveal|todo|+emacs/org
ox-twbs|todo|+emacs/org
p4|todo|+source-control/perforce
pacmacs|todo|+fun/games
pangu-spacing|todo|+intl/chinese
paradox|todo|+spacemacs/spacemacs-ui
pastels-on-dark-theme|todo|+themes/themes-megapack
pbcopy|todo|+os/osx
pcre2el|todo|+distributions/spacemacs-base
pdf-tools|**DONE**|+tools/pdf-tools
persp-mode|todo|+completion/helm, +completion/ivy, +emacs/org, +spacemacs/spacemacs-layouts, +chat/erc, +chat/rcirc
phoenix-dark-mono-theme|todo|+themes/themes-megapack
phoenix-dark-pink-theme|todo|+themes/themes-megapack
php-auto-yasnippets|todo|+lang/php
php-extras|todo|+lang/php
php-mode|todo|+lang/php
phpcbf|todo|+lang/php
phpunit|todo|+lang/php
pip-requirements|todo|+lang/python
planet-theme|todo|+themes/themes-megapack
pony-mode|todo|+frameworks/django
popup|**DONE**|+spacemacs/spacemacs-ui-visual
popwin|todo|+completion/helm, +tools/geolocation, +spacemacs/spacemacs-ui-visual, +checkers/syntax-checking, +lang/clojure, +lang/elixir, +lang/ruby, +lang/elm
powerline|**DONE**|+vim/vim-powerline
powershell|todo|+lang/windows-scripts
prodigy|todo|+tools/prodigy
professional-theme|todo|+themes/themes-megapack
projectile|**DONE**|+completion/helm, +completion/ivy, +tools/shell, +distributions/spacemacs-base
projectile-rails|todo|+frameworks/ruby-on-rails
psc-ide|todo|+lang/purescript
psci|todo|+lang/purescript
pug-mode|todo|+lang/html
puppet-mode|todo|+tools/puppet
purescript-mode|todo|+lang/purescript
purple-haze-theme|todo|+themes/themes-megapack
py-isort|**DONE**|+lang/python
pyenv-mode|todo|+lang/python
pylookup|todo|+lang/python
pytest|todo|+lang/python
pyvenv|**DONE**|+lang/python
qml-mode|**DONE**|+lang/extra-langs
racer|todo|+lang/rust
racket-mode|todo|+lang/racket
railscasts-theme|todo|+themes/themes-megapack
rainbow-delimiters|**DONE**|+spacemacs/spacemacs-editing-visual
rainbow-identifiers|**DONE**|+themes/colors
rainbow-mode|**DONE**|+themes/colors
rake|todo|+lang/ruby
ranger|todo|+vim/evil-snipe, +tools/ranger
rase|todo|+tools/geolocation
rbenv|todo|+lang/ruby
rcirc|todo|+chat/rcirc
rcirc-color|todo|+chat/rcirc
rcirc-notify|todo|+chat/rcirc
request|todo|+spacemacs/spacemacs-misc
restart-emacs|**DONE**|+spacemacs/spacemacs-ui
restclient|todo|+tools/restclient
restclient-helm|todo|+tools/restclient
reveal-in-osx-finder|todo|+os/osx
reverse-theme|todo|+themes/themes-megapack
robe|todo|+lang/ruby
rspec-mode|todo|+lang/ruby
rubocop|todo|+lang/ruby
ruby-test-mode|todo|+lang/ruby
ruby-tools|todo|+lang/ruby
rust-mode|**DONE**|+lang/rust
rvm|todo|+lang/ruby
sass-mode|todo|+lang/html
sbt-mode|todo|+lang/scala
scad-mode|todo|+lang/extra-langs
scala-mode|todo|+lang/scala
scss-mode|todo|+lang/html
selectric-mode|todo|+fun/selectric
seti-theme|todo|+themes/themes-megapack
shell-pop|todo|+tools/shell
skewer-mode|todo|+lang/javascript
slim-mode|todo|+lang/html
slime|todo|+lang/common-lisp
smartparens|todo|+completion/auto-completion, +frameworks/react, +spacemacs/spacemacs-editing, +lang/python, +lang/clojure, +lang/latex, +lang/elixir, +lang/ruby, +lang/html, +lang/ocaml, +lang/markdown, +lang/emacs-lisp, +lang/sml, +lang/elm
smeargle|**DONE**|+source-control/git
smex|**DONE**|+completion/ivy, +emacs/smex
sml-mode|**DONE**|+lang/sml
smyx-theme|todo|+themes/themes-megapack
soft-charcoal-theme|todo|+themes/themes-megapack
soft-morning-theme|todo|+themes/themes-megapack
soft-stone-theme|todo|+themes/themes-megapack
solarized-theme|**DONE**|+themes/themes-megapack
soothe-theme|todo|+themes/themes-megapack
spacegray-theme|todo|+themes/themes-megapack
spaceline|todo|+spacemacs/spacemacs-ui-visual, +spacemacs/spacemacs-layouts
spacemacs-whitespace-cleanup|todo|+spacemacs/spacemacs-editing
spotify|todo|+web-services/spotify
spray|todo|+tools/speed-reading
sql sql-indent|todo|+lang/sql
srefactor|todo|+emacs/semantic
stan-mode|todo|+lang/extra-langs
stickyfunc-enhance|todo|+emacs/semantic, +lang/python
subatomic-theme|todo|+themes/themes-megapack
subatomic256-theme|todo|+themes/themes-megapack
sublime-themes|todo|+themes/themes-megapack
sudoku|todo|+fun/games
sunny-day-theme|todo|+themes/themes-megapack
sunshine|todo|+tools/geolocation
swift-mode|todo|+lang/swift
swiper|**DONE**|+completion/ivy
tagedit|todo|+lang/html
tango-2-theme|todo|+themes/themes-megapack
tango-plus-theme|todo|+themes/themes-megapack
tangotango-theme|todo|+themes/themes-megapack
tao-theme|todo|+themes/themes-megapack
tern|todo|+frameworks/react, +lang/javascript
terraform-mode|todo|+tools/terraform
theme-changer|todo|+tools/geolocation
thrift|todo|+lang/extra-langs
tide|todo|+lang/typescript
tildify|todo|+emacs/typography
tmux|todo|+tools/tmux
toml-mode|todo|+lang/rust
toxi-theme|todo|+themes/themes-megapack
tronesque-theme|todo|+themes/themes-megapack
tuareg|todo|+lang/ocaml
twilight-anti-bright-theme|todo|+themes/themes-megapack
twilight-bright-theme|todo|+themes/themes-megapack
twilight-theme|todo|+themes/themes-megapack
twittering-mode|todo|+web-services/twitter
typescript-mode|todo|+lang/typescript
typit|todo|+fun/games
typo|todo|+emacs/typography, +lang/latex
ujelly-theme|todo|+themes/themes-megapack
underwater-theme|todo|+themes/themes-megapack
undo-tree|**DONE**|+spacemacs/spacemacs-editing
use-package|**DONE**|+distributions/spacemacs-bootstrap
utop|todo|+lang/ocaml
uuidgen|todo|+spacemacs/spacemacs-editing
vi-tilde-fringe|todo|+vim/vim-empty-lines, +tools/shell, +spacemacs/spacemacs-evil
vim-empty-lines-mode|todo|+vim/vim-empty-lines
vim-powerline|todo|+vim/vim-powerline
vimrc-mode|todo|+lang/vimscript
vmd-mode|todo|+lang/markdown
volatile-highlights|todo|+spacemacs/spacemacs-editing-visual
wakatime-mode|todo|+web-services/wakatime
web-beautify|todo|+frameworks/react, +lang/javascript
web-mode|todo|+frameworks/react, +lang/html, +lang/typescript
wgrep|todo|+completion/ivy
which-key|**DONE**|+frameworks/ruby-on-rails, +tools/speed-reading, +lang/latex, +distributions/spacemacs-bootstrap
wolfram-mode|todo|+lang/extra-langs
x86-lookup|todo|+lang/asm
xcscope|todo|+tags/cscope, +lang/python
xkcd|todo|+fun/xkcd
xterm-color|todo|+tools/shell
yapfify|todo|+lang/python
yasnippet|**DONE**|+completion/auto-completion, +lang/latex, +lang/html
ycmd|todo|+tools/ycmd
youdao-dictionary|todo|+intl/chinese
zen-and-art-theme|todo|+themes/themes-megapack
zenburn-theme|**DONE**|+themes/themes-megapack
zonokai-theme|todo|+themes/themes-megapack
"""]]
